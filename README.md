# python multiprocess dispatcher
_Parallelize your python function with a multiprocess wrapper._

## Features:

Allow some function(s) of yours to be parallelized;  
get the different result and error(s) for each function once the dispatcher has finished.

During runtime, you can print to the console and still have a human readable output.

## Smooth distribution
Indicating a maximum number of threads to be running at the "same time", the dispatcher will handle the starting of the threads for you, keeping the other ones in a queue, and starting them as soon as there are eligible to run.

It is possible to choose 2 types of process:
- `process`: 
to run a python function.

- `subprocess`
to launch a python subprocess or an exernal program like an .exe .

- `class_method`
to launch some method of a class instance.
the usual explicit "self" argument indicated as parameter in enqueue.

## Logging, return values and errors:
_Avoid scrambled printing:_  
Typically, we can't predict when the printing execution in your function occurs, especially when printing multiple lines to the console.  
2 or more threads running in parallel and printing multilines at the same time might generate incomprehensible text...

To prevent this , each distributed task will receive two queues ( msg_queue , err_queue )  
in order to send information to the dispacher which will be in charge of logging this information.

#### Sending information to the dispacher:

To send an information from the distributed function, your message will be formatted this way:  
- `msg_queue.put( [ idx , "creating receipe"] )`
- `err_queue.put( [ idx , "no more beans !"] )`

it will be logged on the fly by the dispacher.

### The result and error Queues:


#### Results:
When it finishes, a distributed task can return its result(s) to the dispacher.  
Once all task have finished, the dispacher returns a list containing every task's index, result and error.

_( Please note that although we save errors to display them later,  
 the messages sent to msg_queue are printed on the fly and not kept. )_

As an example, the result of available receipes in the restaurant:  
Each task is picking some random aliments from a shelf and make a receipe:  
result =  
`0 ['egg','bacon'] `  
`1 ['egg','sausage', bacon'] `  
`2 ['egg','SPAM'] `  
`3 ['egg','bacon',SPAM'] `   
`...`  
`10 ['lobster','thermodor', crevette', 'mornay_sauce' , ... ,'SPAM'] `

- It is possible to mix up task that returns something with tasks that doesn't return anything.  
Nonetheless, tasks that doesn't return any value will still insert a `None` element in the result Queue


## Usage
- init the dispatcher class:  
`dp = dispatcher( max_thread = 2)`  
to have at most 2 threads running at the same time.  
The other one(s) waiting for eligibility. 

- enqueue your functions:  
`tp.enqueue( my_function, some_arguments )`  
`tp.enqueue( ur_function, other_arguments )`  
`tp.enqueue( her_function, other_arguments )`

- run them with the dispatcher, watch them run, get results and hopefully not,  errors:  
`    res, err =tp.run( )`  

- read results _( if any )_ after everything is done:   
`for _res in res:`  
`    logger.info( '%3d : %s',_res[0], res[1], extra=d )`

- read errors _( if any )_ afterwards  
`for _err in err:`  
`    logger.error( '%3d : %s',_err[0], _err[1], extra=d )`

### Arguments of your task
in order to run as desired, the task to be distributed has to handle some arguments:
- _( mandatory )_ First argument is personal and mandatory.  
    * Arguments will be wrapped into a list.
    * If no arguments, please fill placeholder with `None`.
- _( optional  )_ err_queue
    The error queue, displayed on the fly and kept for summary.
- _( optional  )_ msg_queue
    The message queue, displayed on the fly but not kept for summary

### Simple usage example:
Please find a simple running example embeded in the source code.  
to test it, simply run `python demo_spam.py`in the terminal.

Highly inspired by the Monthy Python's "egg & spam" sketch,  
the example shows how simply we can create a list of recipes in parallel.  
Each recipe is created by a multiprocess.Process with the help of the available ingredients  
_( a dictionary describing ingredient and its probability of insertion into the recipe )._

## Limitations
This is a very simple dispatcher and quite regrettably, only a few behaviors are implemented.  
A few examples of what this dispatcher can't do are cited hereunder:

### No input()
This handler is not capable to handle user inputs.  
ex: `input( "Have you got anything without SPAM ?" )` in your task can't be handled.   

### No Barrier
The dispatcher is not capable to handle barriers.  
One process in the line can not wait for another to finish, nor to reach a checkpoint.


### Not on the fly
Once started, new tasks can't safely be added/enqueued.  
This is due to the fact that once all threads waiting have been launched,  
the dispatcher will not watch again if a new task has been addded.

However, if you know that there is still some task waiting,  
it is still possible to do so, but there is no garanty that the task will be launched.  
