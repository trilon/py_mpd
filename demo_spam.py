###################
#demo
###################
from mpd import dispacher
from mpd_log import logger
from mpd_log import setFormatter
from mpd_log import logging

import random
import time


"""
This demo file parallelizes the creation of recipe book 
Each task takes randomly some ingredient from a list
procrastinating between each take.
"""

ingredients = { 
    "SPAM"        : 40,
    "bacon"       : 12,
    "eggs"        :  9,
    "sausage"     :  8,
    "crevettes"   :  5,
    "thermodor"   :  4,
    "mornay_sauce":  7,
    "beans"       :  5,
    }

def get_recipe( ingredients, queues = None, idx = 0 ):
    altogether = []
    recipe = []
    

    queues.debug.put( [idx, ' picking all ingredients'])
    for ingr, quant in ingredients.items():
        for q in range(quant):
            altogether.append( ingr )
    random.shuffle( altogether )

    queues.debug.put( [idx, ' creating recipe'] )
    for i in range  ( int( random.uniform( 3, 10 ) ) ):
        #time.sleep(  random.uniform( 0.1, 3 ) ) # procrastination
        random_product = altogether[ int( random.uniform(0, len( altogether )-1) )]
        try:
            if random_product == "beans":
                raise KeyError
            queues.debug.put( [idx, ' picked:'+ random_product ])
            recipe.append(  random_product )

        except KeyError as e:    
            queues.error.put( [ idx , "We don't have beans anymore" ] ) 
            recipe.append(  'WONDEFULL_SPAM' )

    return recipe


def spam_n_eggs():
    import datetime, time
    #logger.setLevel(logging.INFO)
    d = {'dispacher': __name__ }
    setFormatter('%(levelname)-8s :: %(dispacher)-8s :: %(message)s')
    
    startTime = datetime.datetime.now()
    mp = dispacher( max_thread = 4, name= 'mpd_ktchn')

    mp.enqueue( 'process', get_recipe, ingredients )
    mp.enqueue( 'process', get_recipe, ingredients )
    mp.enqueue( 'process', get_recipe, ingredients )
    mp.enqueue( 'process', get_recipe, ingredients )

    res, err =mp.run( )
    
    endTime = datetime.datetime.now()
    logger.info( 'Exec time: %s',str(endTime - startTime), extra=d )

    logger.info( '** ERRORS **:', extra=d )
    for _err in err:
        logger.error( '%3d : %s',_err[0], _err[1], extra=d )
    if len( err) ==0:
        logger.info( '\tNo errors', extra=d )

    logger.info( '** RESULTS **', extra=d )
    for _res in res:
        logger.info( '%3d : %s',_res[0], _res[1], extra=d )

if __name__ =='__main__':
    spam_n_eggs()