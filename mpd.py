#!/bin/python

import os, sys
import time

import sys
import time
import traceback

from datetime import datetime

#import threading
# ! threading is not parallel because of GIL.
# we use multiprocessing instead
from multiprocessing import Process, Queue
import subprocess

import ast

import logging
from mpd_log import logger

class Queues:
    __slots__ = ('info',
                'debug',
                'warning',
                'error',
                'critical',
                'result'
                )
    def __init__(self):
        self.info    = Queue()
        self.debug   =Queue()
        self.warning = Queue()
        self.error   = Queue()
        self.critical= Queue()
        self.result  =Queue()

class dispacher():
    def __init__( self, max_thread=1, name = 'Dispacher' ):

        #self.logger = logging.getLogger('mpd_log.mod2')

        self.max_thread = max_thread # max number of process to be running at the same time.
        self.name = name # Name of the distributor.
        self.queued = [] # queue holding Functions to be launched distibutetly.
        self.res = []
        self.err = []

        d = {'dispacher': self.name }
        logger.info('Init dispacher:', extra = d)
        logger.info('max_threads= '+str(max_thread), extra = d)


            
        self.queues = Queues()
        


    def _distr_function(self , idx, user_function, fn_args ):
        # Launch an independent python task ( distributed )
        start_time = datetime.now()
        self.queues.debug.put( [idx , user_function.__name__ +' started: ' + str( start_time )] )

        try:
            process_time =  str( datetime.now() - start_time )
            self.queues.result.put( [ idx , user_function( fn_args, self.queues, idx = idx ) ] )
            self.queues.info.put( [ idx , user_function.__name__ +' done: '+ process_time ] )

        except Exception as e:
            self.queues.error.put( [ idx , str(e)+"\n"+ str( traceback.format_exc() ) ] ) 

    def _distr_method(self, idx, user_function, fn_class_self,  fn_args ):
        # Launch class method ( distributed )
        start_time = datetime.now()
        self.queues.debug.put( [idx , user_function.__name__ +' started: ' + str( start_time ) ] )
        
        try:
            process_time =  str( datetime.now() - start_time )
            self.queues.result.put( [ idx , user_function( fn_class_self , fn_args, self.queues, idx = idx) ] )
            self.queues.debug.put( [ idx, user_function.__name__ +' done: '+ process_time ])

        except Exception as e:
            self.queues.error.put( [ idx , str(e)+"\n"+ str( traceback.format_exc() ) ] ) 

    def _distr_subprocess( self , idx, user_command, fn_args ):
        # Launch an autonomous subprocess ( distributed )

        verbose = True
        process = subprocess.Popen(user_command,
                                    shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)

        start_time = datetime.now()

        self.queues.debug.put( [idx, 'command' +' started: ' + str( start_time )] )

        while True:
            nextline = process.stdout.readline().decode("utf-8")
            if nextline == '' and process.poll() is not None:
                break
               
                def format_line( nexline ):
                    if nextline[-1:] == '\n':
                        return nextline[:-1]
                    else:
                        return nextline

                nextline_form = format_line( nextline)
                self.queues.info.put( [ idx,  nextline_form ] )

        output = process.communicate()[0]
       
        exitCode = process.returncode

        if (exitCode == 0):
            if verbose == True:
                pass
        else:
            self.queues.errors.put( [ idx , str(e)+"\n"+ + str(exitCode) + " " + str(output) ] )

        if verbose == True:
            process_time =  str( datetime.now() - start_time )
            self.queues.debug.put( [idx , user_command +' done: '+ process_time ] )


    def enqueue(self, fn_type ,function, args ):
        self.queued.append( [ fn_type, function, args] )
        
    def _isAlive(self, threads_list):
        for trd in threads_list:
            d = {'dispacher': self.name }

            if trd.is_alive():
                for name in self.queues.__slots__:
                    queue = getattr( self.queues , name )
                    if not queue.empty():
                        msg = queue.get( False )
                        mthd = getattr( logger, name ) 
                        mthd( '%3d : Alive: %s', msg[0], msg[1], extra=d )

                        if name == 'error':
                            self.err.append( msg )

                        elif name == 'result':
                            self.res.append( msg )
            else:
                for name in self.queues.__slots__:
                    queue = getattr( self.queues , name )
                    if not queue.empty():
                        msg = queue.get( False )
                        logger.debug( '%3d : Dead : %s', msg[0], msg[1], extra=d )
                threads_list.remove( trd )



    def run( self ):
        d = {'dispacher': self.name }
        logger.info('Start run():', extra = d)


        threads_list=[]

        for idx in range( len( self.queued )  ):
            # if max threads are lauched, do not launch until process is done
            while len( threads_list ) >= self.max_thread:
                self._isAlive( self.queues ,threads_list)
            else:
                logger.debug( '%3d : %s', idx, 'Creating', extra=d )
                if self.queued[ idx ][0] == 'process':
                    trd = Process(
                                target =  self._distr_function ,
                                args= ( idx  , self.queued[ idx ][1] ,self.queued[ idx ][2] , )
                                )

                elif self.queued[ idx ][0] == 'subprocess':
                    trd = Process(
                                target =  self._distr_subprocess ,
                                args= (   idx  , self.queued[ idx ][1] ,self.queued[ idx ][2] , )
                                )

                elif self.queued[ idx ][0] == 'class_method':
                    trd = Process(
                                target =  self._distr_method ,
                                args= (   idx  , self.queued[ idx ][1] , self.queued[ idx ][2] , self.queued[ idx ][3] , )
                                )

                trd.daemon= True
                threads_list.append( trd )

                trd.start()
                #logger.debug('Started ['+ '{:3}'.format( str( idx ).zfill(3) ) +'] total running = '+ str(len( threads_list )), extra =d )
                logger.debug( '%3d : %s',idx, 'started', extra=d )
                

        # all threads have been launched
        # wait fot them to finish while getting their msg_queue
        logger.info('all threads launched... waiting to finish', extra=d)
        while len(threads_list) > 0:
            self._isAlive( threads_list)
        
        logger.info('All done', extra=d)
        
        return self.res, self.err

