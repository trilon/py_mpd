import logging
import sys




logging.RESULT = 999
logging.addLevelName(logging.RESULT, 'RESULT')

class MyLogger(logging.getLoggerClass()):
   def result(self, msg, *args, **kwargs):
       self.log(logging.RESULT, msg, *args, **kwargs)

logging.setLoggerClass(MyLogger)


logger = logging.getLogger( 'mpd_log')
logger.setLevel( logging.DEBUG )

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(levelname)-8s%(process)d[%(thread)6d] %(module)-5s::%(dispacher)-10s :: %(message)s')

ch.setFormatter(formatter)
logger.addHandler(ch)




def setFormatter( str_format):
    formatter = logging.Formatter( str_format)
    ch.setFormatter(formatter)


#logger.result("hello %s", "Guido", extra={'dispacher': 'mpd_log'})
